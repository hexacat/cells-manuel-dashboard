{
  const {
    html,
  } = Polymer;
  /**
    `<cells-manuel-dashboard>` Description.

    Example:

    ```html
    <cells-manuel-dashboard></cells-manuel-dashboard>
    ```

    ## Styling
    The following custom properties and mixins are available for styling:

    ### Custom Properties
    | Custom Property     | Selector | CSS Property | Value       |
    | ------------------- | -------- | ------------ | ----------- |
    | --cells-fontDefault | :host    | font-family  |  sans-serif |
    ### @apply
    | Mixins    | Selector | Value |
    | --------- | -------- | ----- |
    | --cells-manuel-dashboard | :host    | {} |

    * @customElement
    * @polymer
    * @extends {Polymer.Element}
    * @demo demo/index.html
  */
  class CellsManuelDashboard extends Polymer.Element {

    static get is() {
      return 'cells-manuel-dashboard';
    }

    static get properties() {
      return {
        user: {
          type: String,
          notify: true,
          value: "",
        },
        isLogged: {
          type: Boolean,
          value: false
        },
        list: {
          type: Object,
          value: {},
          notify: true
        }
      };
    }

    ready() {
      super.ready();
      console.log(this.list);
    }

    filter(string) {
      if(!string) {
        return null;
      } else {
        // string= string.toLowerCase();
        return function(list) {
          let search= string.toLowerCase();
          let name= list.name.toLowerCase();
          let last= list.last.toLowerCase();
          return (name.indexOf(search) != -1 || last.indexOf(search) != -1);
        };
      }
    }

    static get template() {
      return html `
      <style include="cells-manuel-dashboard-styles cells-manuel-dashboard-shared-styles"></style>
      <slot></slot>

        <input type="text" value="{{user::input}}" class="filter">
          <div class="card-content">
            <blockquote>Frase: [[quote.value]]</blockquote>
          </div>
          <div class="box">
            <template is="dom-repeat" items="{{list}}" filter="{{filter(user)}}" id="users">
              <div>Name: <span>{{item.name}}</span></div>
              <div>Surname: <span>{{item.last}}</span></div>
              <div>Address: <span>{{item.address}}</span></div>
              <div>Hobbies: <span>{{item.hobbies}}</span></div>
            </template>
          </div>
        <cells-mocks-component alumnos="{{list}}"></cells-mocks-component>
        <iron-ajax
          auto
          id="req"
          url="https://api.chucknorris.io/jokes/random"
          method="get"
          handle-as="json"
          last-response="{{quote}}">
        </iron-ajax>
      `;
    }
  }

  customElements.define(CellsManuelDashboard.is, CellsManuelDashboard);
}
